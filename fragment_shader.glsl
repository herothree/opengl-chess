#version 410


// Get this from the previous (vertex) stage
//in vec3 normal;
in float shading;
//in vec2 st;

// Grey 0.1647, 0.1647, 0.1647, 0.1647
// Cream: 1.0, 0.95294, 0.737254, 1.0

// First vec4 output from fragment shader is automatically used
// as the color by opengl. Should have values between 0 and 1.
out vec4 frag_color;

void main()
{

    // Start with a light cream to represent the white pieces
    frag_color = shading * vec4(0.1647, 0.1647, 0.1647, 0.1647); // grey
    //frag_color = shading * vec4(1.0, 0.95294, 0.737254, 1.0); // cream
    //frag_color = vec4(color, 1.0);
    //frag_color = vec4(normal, 1.0);
}



