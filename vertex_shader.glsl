#version 410

// These are different for each vertex
// these locations should match the ones in glVertexAttribPointer
// This variable comes from location 0
layout(location = 0) in vec3 vertex_position;

// This variable comes from location 1
layout(location = 1) in vec3 vertex_normal;

layout(location = 2) in vec2 texture_coord;

// These are the same for each vertex
// uniform basically means "global in internal opengl"
uniform mat4 view, proj, model;
uniform vec3 cam_pos;

uniform float Ka;
uniform float Kd;
uniform float Ks;
uniform float alpha;
uniform float shininess;
uniform vec3 light_dir;

// send this variable to the next stage
//out vec3 normal;
out float shading;
//out vec2 st;

void main()
{
    vec3 view_dir = vertex_position - cam_pos;
    normalize(view_dir);

    //st = texture_coord;
    //normal = vertex_normal;
    gl_Position = model * proj * view * vec4(vertex_position, 1.0);

    // Calculate shading
    //shading = Ka + Kd*Diffuse + Ks*Specular
    shading = 0.0;

    // Calculate the ambient component of the lighting as simply the ambient parameter
    shading += Ka;

    // Calculate the diffuse component of the lighting as the dot produce of L and N
    // Use abs to achieve two sided lighting, or max(0, x) for one sided.
    float diffuse = max(0, dot(vertex_normal, light_dir));

    shading += Kd * diffuse;

    vec3 r = vertex_normal.xyz;
    r = 2 * dot(light_dir, vertex_normal) * vertex_normal - light_dir;

    // Calculate the specular component of the lighting

    // Make sure not to raise a negative dp to a power
    // view_dir = vertex position - camera position
    float dp = dot(view_dir, r);
    float specular = 0;
    if (dp > 0)
    {
        specular = max(0.0, shininess * pow(dp, alpha));
    }
    else
    {
        specular = 0.0;
    }

    shading += Ks * specular;
}
