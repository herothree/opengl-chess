// Include GLFW
#include <GLFW/glfw3.h>
extern GLFWwindow* window; // The "extern" keyword here is to access the variable "window" declared in tutorialXXX.cpp. This is a hack to keep the tutorials simple. Please avoid this.

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdio.h>

using namespace glm;


#include "controls.hpp"

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;

glm::mat4 getViewMatrix(){
    return ViewMatrix;
}
glm::mat4 getProjectionMatrix(){
    return ProjectionMatrix;
}


// Initial position : on +Z
glm::vec3 position = glm::vec3( 3, 3, 8 ); 

// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;

// Initial vertical angle : none
float verticalAngle = 0.0f;

// Initial Field of View
float initialFoV = 45.0f;

float speed = 3.0f; // 3 units / second
float mouseSpeed = 0;//0.005f;
float cameraRotateSpeed = 0.005f;

extern double m_xpos;
extern double m_ypos;

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
    {
        // Get mouse position
        glfwGetCursorPos(window, &m_xpos, &m_ypos);

        // Change our convention to be from the bottom of the screen instead of the top.
        m_ypos = 768.0 - m_ypos;
    }
}

void computeMatricesFromInputs(){

    // glfwGetTime is called only once, the first time this function is called
    static double lastTime = glfwGetTime();

    // Compute time difference between current and last frame
    double currentTime = glfwGetTime();
    float deltaTime = float(currentTime - lastTime);

    // Get mouse position
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    // Reset mouse position for next frame
    //glfwSetCursorPos(window, 1024/2, 768/2);

    // Compute new orientation
    horizontalAngle += mouseSpeed * float(1024/2 - xpos );

    // Control the camera direction with the arrow keys
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {
        verticalAngle += cameraRotateSpeed;// * float( 768/2 - ypos );
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {
        verticalAngle -= cameraRotateSpeed;// * float( 768/2 - ypos );
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {
        horizontalAngle += cameraRotateSpeed;// * float( 768/2 - ypos );
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
    {
        horizontalAngle -= cameraRotateSpeed;// * float( 768/2 - ypos );
    }

    // Compute the current direction of the camera.
    // Direction : Spherical coordinates to Cartesian coordinates conversion
    glm::vec3 direction(
            cos(verticalAngle) * sin(horizontalAngle), 
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
            );

    // Right vector
    glm::vec3 right = glm::vec3(
            sin(horizontalAngle - 3.14f/2.0f), 
            0,
            cos(horizontalAngle - 3.14f/2.0f)
            );

    // Up vector
    glm::vec3 up = glm::cross( right, direction );

    // Move forward with Q
    if (glfwGetKey( window, GLFW_KEY_Q ) == GLFW_PRESS){
        position += direction * deltaTime * speed;
    }
    // Move backward with E
    if (glfwGetKey( window, GLFW_KEY_E ) == GLFW_PRESS){
        position -= direction * deltaTime * speed;
    }

    // Strafe up
    if (glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS){
        position += up * deltaTime * speed;
    }
    // Strafe Down
    if (glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS){
        position -= up * deltaTime * speed;
    }
    // Strafe right
    if (glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS){
        position += right * deltaTime * speed;
    }
    // Strafe left
    if (glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS){
        position -= right * deltaTime * speed;
    }

    float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

    // Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 100.0f);
    // Camera matrix
    ViewMatrix       = glm::lookAt(
            position,           // Camera is here
            position+direction, // and looks here : at the same position, plus "direction"
            up                  // Head is up (set to 0,-1,0 to look upside-down)
            );

    // For the next frame, the "last time" will be "now"
    lastTime = currentTime;
}

