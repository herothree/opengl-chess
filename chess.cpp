#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <thread>

#include "engine/chess.h"

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#ifdef APPLE
#include <GLFW/glfw3.h>
#else
#include <glfw3.h>
#endif
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "common/shader.hpp"
#include "common/texture.hpp"
#include "common/controls.hpp"
#include "common/objloader.hpp"
#include "common/vboindexer.hpp"

#define BLACK (1)
#define WHITE (2)

// Signals that a piece is captured and should not be drawn
#define CAPTURED (-1934) 

// 16 white, 16 black, 64 squares
#define NUM_PIECES 96

const char *piece_filenames[] = {
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",
    "Pawn.obj",

    "Rook.obj",
    "Knight.obj",
    "Bishop.obj",
    "Queen.obj",
    "King.obj",
    "Bishop.obj",
    "Knight.obj",
    "Rook.obj",

    "Square.obj"
};

/*
 * Contains information about the vertices of a piece.
 */
class GLPiece
{
    public:
        GLPiece(int color);
        ~GLPiece();
        std::vector<unsigned short> *indices;
        std::vector<glm::vec3> *vertices;
        std::vector<glm::vec2> *uvs;
        std::vector<glm::vec3> *normals;
        int color;
        bool captured;
        int _rank; // row
        int _file; // column
    private:
};

GLPiece::GLPiece(int color)
{
    this->captured = false;
    this->color = color;

    this->indices = new std::vector<unsigned short>();
    this->vertices = new std::vector<glm::vec3>();
    this->normals = new std::vector<glm::vec3>();
    this->uvs = new std::vector<glm::vec2>();

    // -1 -> not on board
    this->_rank = CAPTURED;
    this->_file = CAPTURED;
}

GLPiece::~GLPiece()
{
    //printf("Goodbye cruel world!\n");
    return;

    // TODO: Memory leak here?
}

// Each of these contains an array of ints, and the int is a pointer to internal opengl
GLuint vertexbuffers[NUM_PIECES];
GLuint uvbuffers[NUM_PIECES];
GLuint normalbuffers[NUM_PIECES];
GLuint elementbuffers[NUM_PIECES];

vec3 cream(1.0, 0.95294, 0.737254); // Cream
vec3 grey (0.1647, 0.1647, 0.1647); // Grey

// Use this as a flag; if it's not -1, that means the mouse was clicked.
double m_xpos = -1;
double m_ypos = -1;

int startX;
int startY;
int endX;
int endY;
volatile bool moved;

void drawPiece(int i, glm::mat4 ModelMatrix1, vec3 color, GLuint LightID, GLuint ColorID, GLuint ViewMatrixID, GLuint MatrixID, GLuint ModelMatrixID,
        GLuint Texture, GLuint TextureID, std::vector<GLPiece> pieces, GLuint programID)
{
    glm::mat4 ProjectionMatrix = getProjectionMatrix();
    glm::mat4 ViewMatrix = getViewMatrix();

    // If the pieces are the white knights, they need to be turned around
    if (i == 9 || i == 14)
    {
        ModelMatrix1 = rotate(ModelMatrix1, 3.14159f, vec3(0,1,0));
    }

    // Use our shader
    glUseProgram(programID);


    // Save the piece color in internal opengl
    glUniform3f(ColorID, color.x, color.y, color.z);

    glm::vec3 lightPos = glm::vec3(4,4,4);
    glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);
    glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]); // This one doesn't change between objects, so this can be done once for all objects that use "programID"

    glm::mat4 MVP1 = ProjectionMatrix * ViewMatrix * ModelMatrix1;

    // Send our transformation to the currently bound shader,
    // in the "MVP" uniform
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP1[0][0]);
    glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix1[0][0]);


    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture);
    // Set our "myTextureSampler" sampler to user Texture Unit 0
    glUniform1i(TextureID, 0);

    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[i]);
    glVertexAttribPointer(
            0,                  // attribute
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
            );

    // 2nd attribute buffer : UVs
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffers[i]);
    glVertexAttribPointer(
            1,                                // attribute
            2,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
            );

    // 3rd attribute buffer : normals
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffers[i]);
    glVertexAttribPointer(
            2,                                // attribute
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
            );

    // Index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffers[i]);

    // Draw the triangles !
    glDrawElements(
            GL_TRIANGLES,      // mode
            pieces[0].indices->size(),    // count
            GL_UNSIGNED_SHORT,   // type
            (void*)0           // element array buffer offset
            );

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

/*
 ******************************************************************
 * Main
 ******************************************************************
 */
int chess_graphics(void)
{
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( 1024, 768, "Chess", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Hide the mouse and enable unlimited mouvement
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set up a mouse button callback so we can select pieces.
    glfwSetMouseButtonCallback(window, mouse_button_callback);


    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);

    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);

    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    //glEnable(GL_CULL_FACE);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    //GLuint pickingProgramID = LoadShaders( "picking.vertexshader", "picking.fragmentshader" );
    GLuint programID = LoadShaders( "StandardShading.vertexshader", "StandardShading.fragmentshader" );
    //GLuint programID = pickingProgramID;

    // Get a handle for our "MVP" uniform
    GLuint MatrixID = glGetUniformLocation(programID, "MVP");
    GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
    GLuint ModelMatrixID = glGetUniformLocation(programID, "M");

    // Load the texture
    GLuint Texture = loadDDS("uvmap.DDS");

    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");

    std::vector<GLPiece> pieces;


    int the_color = WHITE;

    // Read the pieces
    for (int i = 0; i < NUM_PIECES; ++i)
    {
        GLPiece piece(the_color);
        // The 16th filename is the square
        if (!loadAssImp(piece_filenames[(i >= 32) ? 16 : (i % 16)], *(piece.indices), *(piece.vertices), *(piece.uvs), *(piece.normals)))
        {
            fprintf(stderr, "Error reading %s. Aborting.\n", piece_filenames[i]);
            return 1;
        }
        pieces.push_back(piece);

        if (16 == i)
        {
            the_color = BLACK;
        }

        glGenBuffers(1, &(vertexbuffers[i]));
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[i]);
        glBufferData(GL_ARRAY_BUFFER, piece.vertices->size() * sizeof(glm::vec3), &((*(piece.vertices))[0]), GL_STATIC_DRAW);

        glGenBuffers(1, &(uvbuffers[i]));
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffers[i]);
        glBufferData(GL_ARRAY_BUFFER, piece.uvs->size() * sizeof(glm::vec2), &((*(piece.uvs))[0]), GL_STATIC_DRAW);

        glGenBuffers(1, &(normalbuffers[i]));
        glBindBuffer(GL_ARRAY_BUFFER, normalbuffers[i]);
        glBufferData(GL_ARRAY_BUFFER, piece.normals->size() * sizeof(glm::vec3), &((*(piece.normals))[0]), GL_STATIC_DRAW);

        // Generate a buffer for the indices as well
        glGenBuffers(1, &(elementbuffers[i]));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffers[i]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, piece.indices->size() * sizeof(unsigned short), &((*(piece.indices))[0]), GL_STATIC_DRAW);
    }

    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);
    GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

    glUseProgram(programID);
    GLuint ColorID = glGetUniformLocation(programID, "pieceColor");

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0;

    // Set up the board
    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            //ModelMatrix1 = glm::translate(glm::mat4(1.0), vec3(i, 0.1, -j));
            pieces[32+8*i+j]._rank = -j;
            pieces[32+8*i+j]._file = i;

            // Alternate white and black squares
            if ((i + j) % 2 == 1)
            {
                pieces[32+8*i+j].color = WHITE;
                //drawPiece(32, ModelMatrix1, cream, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
            }
            else
            {
                pieces[32+8*i+j].color = BLACK;
                //drawPiece(32, ModelMatrix1, grey, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
            }
        }
    }

    // Set up the black pieces
    for (int i = 0; i < 2; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            //ModelMatrix1 = glm::translate(glm::mat4(1.0), vec3(j, 0, -1 + i));
            pieces[i*8+j]._file = j;
            pieces[i*8+j]._rank = -1+i;
            pieces[i*8+j].color = WHITE;
            //drawPiece(8 * i + j, ModelMatrix1, cream, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
        }
    }

    // Set up the black pieces
    for (int i = 0; i < 2; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            pieces[16 + i*8+j]._file = j;
            pieces[16 + i*8+j]._rank = -6-i;
            pieces[16 + i*8+j].color = BLACK;

            //ModelMatrix1 = glm::translate(glm::mat4(1.0), vec3(j, 0, -6-i));
            //drawPiece(16 + 8 * i + j, ModelMatrix1, grey, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
        }
    }

    do{

        // Measure speed
        double currentTime = glfwGetTime();
        nbFrames++;
        if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1sec ago
            // printf and reset
            //printf("%f ms/frame\n", 1000.0/double(nbFrames));
            nbFrames = 0;
            lastTime += 1.0;
        }

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // Compute the MVP matrix from keyboard and mouse input
        computeMatricesFromInputs();


        glm::mat4 ModelMatrix1 = glm::mat4(1.0);


        vec3 id_color(0,0,0);

        // Now each piece knows its location and color
        for (int i = 0; i < NUM_PIECES; ++i)
        {
            // Squares should be raised up a bit to make sure they reach the pieces.
            ModelMatrix1 = glm::translate(glm::mat4(1.0), vec3(pieces[i]._file, (i >= 32) ? 0.1 :  0, pieces[i]._rank));

            // Don't display a captured piece
            if (CAPTURED == pieces[i]._rank)
            {
                continue;
            }

            if (WHITE == pieces[i].color)
            {
                drawPiece(i, ModelMatrix1, cream, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
            }
            else
            {
                drawPiece(i, ModelMatrix1, grey, LightID, ColorID, ViewMatrixID, MatrixID, ModelMatrixID, Texture, TextureID, pieces, programID);
            }
        }

        if (moved)
        {
            int firstPiece;
            int capturedPiece;

            // Find the piece on the start move square

            firstPiece = -1;
            for (int i = 0; i < NUM_PIECES && (-1 == firstPiece); ++i)
            {
                if (startX == pieces[i]._file && startY == pieces[i]._rank * -1)
                {
                    firstPiece = i;
                    printf("Found first piece! %d\n", firstPiece);
                }
            }
            
            // Find if there's a piece on the end move square
            capturedPiece = -1;

            // Only go to 32 because after that are squares that we
            // don't want to remove.
            for (int i = 0; i < 32 && (-1 == capturedPiece); ++i)
            {
                if (endX == pieces[i]._file && endY == pieces[i]._rank * -1)
                {
                    capturedPiece = i;
                    printf("Found captured piece! %d\n", capturedPiece);
                }
            }
            
            // If there is, remove it from the board
            if (-1 != capturedPiece)
            {
                pieces[capturedPiece]._rank = CAPTURED;
                pieces[capturedPiece]._file = CAPTURED;
            }

            // Give the first piece new coordinates
            pieces[firstPiece]._rank = endY * -1;
            pieces[firstPiece]._file = endX;

            // If the piece was a white pawn and reached the last row, turn it
            // into a queen.
            if ((firstPiece >= 0 && firstPiece <= 7) && endY == 7)
            {
                /*
                for (int k = 0; k < 32; ++k)
                {
                    printf("%d ", vertexbuffers[k]);
                }
                printf("\n");
                */

                glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[11].vertices->size() * sizeof(glm::vec3), &((*(pieces[11].vertices))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ARRAY_BUFFER, uvbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[11].uvs->size() * sizeof(glm::vec2), &((*(pieces[11].uvs))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ARRAY_BUFFER, normalbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[11].normals->size() * sizeof(glm::vec3), &((*(pieces[11].normals))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffers[firstPiece]);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, pieces[11].indices->size() * sizeof(unsigned short), &((*(pieces[11].indices))[0]), GL_STATIC_DRAW);

                // White queen is index 11
                /*
                vertexbuffers[firstPiece] = vertexbuffers[11];
                uvbuffers[firstPiece] = vertexbuffers[11];
                normalbuffers[firstPiece] = vertexbuffers[11];
                elementbuffers[firstPiece] = vertexbuffers[11];
                */

                printf("\n");
            }

            // If the piece was a black pawn and reached the first row, turn it
            // into a queen.
            if ((firstPiece >= 16 && firstPiece <= 23) && endY == 0)
            {
                // Black queen is index 28
                glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[28].vertices->size() * sizeof(glm::vec3), &((*(pieces[11].vertices))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ARRAY_BUFFER, uvbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[28].uvs->size() * sizeof(glm::vec2), &((*(pieces[11].uvs))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ARRAY_BUFFER, normalbuffers[firstPiece]);
                glBufferData(GL_ARRAY_BUFFER, pieces[28].normals->size() * sizeof(glm::vec3), &((*(pieces[11].normals))[0]), GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffers[firstPiece]);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, pieces[28].indices->size() * sizeof(unsigned short), &((*(pieces[11].indices))[0]), GL_STATIC_DRAW);
            }

        
            moved = false;
        }

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 );

    // Cleanup VBO and shader
    glDeleteBuffers(1, &(vertexbuffers[0]));
    glDeleteBuffers(1, &(uvbuffers[0]));
    glDeleteBuffers(1, &(normalbuffers[0]));
    glDeleteBuffers(1, &(elementbuffers[0]));
    glDeleteProgram(programID);
    glDeleteTextures(1, &Texture);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}

int main(int argc, char* argv[])
{
    std::thread engine(run);
    chess_graphics();

    engine.join();

    return 0;
}

