BIN = chess
CC = g++
FLAGS = -std=c++0x -DAPPLE -Wall -pedantic -mmacosx-version-min=10.12 -arch x86_64 -fmessage-length=0 -UGLFW_CDECL -fprofile-arcs -ftest-coverage -pthread
INC = -I ../common/include -I/sw/include -I/usr/local/include
LIB_PATH = /usr/local/lib/
LOC_LIB = $(LIB_PATH)libGLEW.a $(LIB_PATH)libglfw.3.dylib $(LIB_PATH)libassimp.3.dylib
#LOC_LIB = -lGLEW -lglfw
FRAMEWORKS = -framework Cocoa -framework OpenGL -framework IOKit
SRC = *.cpp common/*.cpp engine/*.cpp

all: run

clean:
	rm -f *.gcda *.gcno chess

build: clean
	${CC} ${FLAGS} ${FRAMEWORKS} -o ${BIN} ${SRC} ${INC} ${LOC_LIB}

run: build
	./chess

	

