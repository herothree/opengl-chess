# README #
Final project for CIS 541, Fall 2016, University of Oregon.
Chess Game  

Initially based on tutorials found at http://www.opengl-tutorial.org/
and more tutorials found at https://capnramses.github.io/opengl/

### How do I get set up? ###

OS X:  
Requirements (install via homebrew):  
        * glew  
        * glfw  
        * glm  
        * assimp  
Then simply execute 

          make build

and 
        ./chess

In the project directory. Exit with ctrl+C

### Controls ###

Enter your move on the terminal in the form startsquare endsquare
(example: D2 D4)

Move the camera up, down, left, and right with W, S, A, D respectively.  
Move the camera forward and backward with Q and E.  
Change the direction of the camera with the arrow keys.  

### Who do I talk to? ###

Jeremy Sigrist  
jsigrist@cs.uoregon.edu
